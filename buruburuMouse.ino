#include <DigiMouse.h>
// #include <CapacitiveButton.h>

// Boards manager URL: http://digistump.com/package_digistump_index.json
// Board: Digispark (Default 16.5MHz)


uint8_t ledPin = 1;
uint8_t sendPin = 2;
uint8_t receivePin = 0;
int8_t yDistance =  2;
int8_t responseDelay = 1;     // response delay of the mouse, in seconds

// CapacitiveButton button = CapacitiveButton(sendPin, receivePin);

//boolean mouseMode = true;
/*
void onButtonPressed(Button& btn){
  mouseMode = !mouseMode;
  digitalWrite(ledPin, HIGH);
}*/

void setup() {
  pinMode(ledPin, OUTPUT);
  DigiMouse.begin();
}

void loop() {
  // if(mouseMode) {
      digitalWrite(ledPin, HIGH);
      DigiMouse.moveY(yDistance);
      yDistance = -yDistance;
      digitalWrite(ledPin, LOW);

  // }
  // button.update();
  for(int i=0; i < responseDelay; i++) {
    DigiMouse.delay(1000);
  }
}
